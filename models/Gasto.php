<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gasto".
 *
 * @property int $id
 * @property int $codReserva
 * @property int $idGasto
 * @property int $descGasto
 * @property int $importeGasto
 *
 * @property Reserva $codReserva0
 */
class Gasto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gasto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codReserva', 'idGasto', 'descGasto', 'importeGasto'], 'integer'],
            [['codReserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reserva::className(), 'targetAttribute' => ['codReserva' => 'codReserva']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codReserva' => 'Cod Reserva',
            'idGasto' => 'Id Gasto',
            'descGasto' => 'Desc Gasto',
            'importeGasto' => 'Importe Gasto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodReserva0()
    {
        return $this->hasOne(Reserva::className(), ['codReserva' => 'codReserva']);
    }
}
