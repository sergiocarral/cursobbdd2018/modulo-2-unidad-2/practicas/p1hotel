<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipodehabitacion".
 *
 * @property int $idTipo
 * @property int $categoria
 * @property string $descripcion
 * @property int $precHabitacion
 *
 * @property Habitacion[] $habitacions
 */
class Tipodehabitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipodehabitacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTipo'], 'required'],
            [['idTipo', 'categoria', 'precHabitacion'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['idTipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTipo' => 'Id Tipo',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
            'precHabitacion' => 'Prec Habitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabitacions()
    {
        return $this->hasMany(Habitacion::className(), ['idTipo' => 'idTipo']);
    }
}
