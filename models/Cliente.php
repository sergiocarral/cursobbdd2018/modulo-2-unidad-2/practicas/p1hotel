<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property string $dni
 * @property string $nombre
 * @property string $apellidos
 * @property string $telefono
 * @property string $email
 * @property string $direccion
 * @property string $fechaNac
 * @property string $poblacion
 * @property string $codPostal
 * @property string $provincia
 * @property string $pais
 *
 * @property Reserva[] $reservas
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['telefono', 'codPostal'], 'number'],
            [['fechaNac'], 'safe'],
            [['dni'], 'string', 'max' => 10],
            [['nombre', 'pais'], 'string', 'max' => 20],
            [['apellidos', 'poblacion', 'provincia'], 'string', 'max' => 40],
            [['email', 'direccion'], 'string', 'max' => 60],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'fechaNac' => 'Fecha Nac',
            'poblacion' => 'Poblacion',
            'codPostal' => 'Cod Postal',
            'provincia' => 'Provincia',
            'pais' => 'Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['dni' => 'dni']);
    }
}
