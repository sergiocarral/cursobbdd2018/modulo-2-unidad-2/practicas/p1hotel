<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $codReserva
 * @property string $fechaEntrada
 * @property string $fechaSalida
 * @property int $numHabitacion
 * @property string $dni
 * @property int $iva
 *
 * @property Gasto[] $gastos
 * @property Cliente $dni0
 * @property Habitacion $numHabitacion0
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaEntrada', 'fechaSalida'], 'safe'],
            [['numHabitacion', 'iva'], 'integer'],
            [['dni'], 'string', 'max' => 10],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['numHabitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Habitacion::className(), 'targetAttribute' => ['numHabitacion' => 'numHabitacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codReserva' => 'Cod Reserva',
            'fechaEntrada' => 'Fecha Entrada',
            'fechaSalida' => 'Fecha Salida',
            'numHabitacion' => 'Num Habitacion',
            'dni' => 'Dni',
            'iva' => 'Iva',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['codReserva' => 'codReserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Cliente::className(), ['dni' => 'dni']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumHabitacion0()
    {
        return $this->hasOne(Habitacion::className(), ['numHabitacion' => 'numHabitacion']);
    }
}
