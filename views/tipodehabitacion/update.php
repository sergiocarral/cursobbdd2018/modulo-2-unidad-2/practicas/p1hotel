<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tipodehabitacion */

$this->title = 'Update Tipodehabitacion: ' . $model->idTipo;
$this->params['breadcrumbs'][] = ['label' => 'Tipodehabitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTipo, 'url' => ['view', 'id' => $model->idTipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipodehabitacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
